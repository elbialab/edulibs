package ru.elbialab.education.solver;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.SolveDrawer;
import ru.elbialab.education.ViewInfo;

public class SolverHome extends SolveDrawer {

   static {
      taskNumber = Integer.MAX_VALUE;
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {

   }

   @Override
   public void onDraw(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xff0000ff);
      canvas.drawLine(40, 40, 40, 120, paint);
      canvas.drawLine(40, 120, 120, 120, paint);
      canvas.drawLine(120, 120,120, 40, paint);

      paint.setColor(0xff7f3f00);
      canvas.drawCircle(80, 25, 10, paint);
      canvas.drawLine(80, 10, 130, 40, paint);
      canvas.drawLine(130, 40, 30, 40, paint);
      canvas.drawLine(30, 40, 80, 10, paint);

      paint.setColor(0xff00ff00);
      canvas.drawRect(60, 60, 100, 100, paint);

   }
}
