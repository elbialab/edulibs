package ru.elbialab.education;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import ru.elbialab.education.tasks.TaskBase;
import ru.elbialab.education.tasks.TaskFactory;
import ru.elbialab.javaedu.R;

public class EducationActivity extends Activity {

   public static SolveDrawer solver;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      setContentView(R.layout.mainlayout);

      final EducationView educationView = findViewById(R.id.task);
      educationView.setSolver(solver);
      setTaskDescription();
      findViewById(R.id.next).setOnClickListener(v -> {
         SolveDrawer.taskNumber++;
         setTaskDescription();
         educationView.onInit();
         findViewById(R.id.prev).setEnabled(true);
      });
      findViewById(R.id.prev).setOnClickListener(v -> {
         SolveDrawer.taskNumber--;
         if (SolveDrawer.taskNumber < 2) {
            v.setEnabled(false);
         }
         educationView.onInit();
         setTaskDescription();
      });
      findViewById(R.id.hide).setOnClickListener(v -> {
         SolveDrawer.showSolve = !SolveDrawer.showSolve;
         ((TextView)v).setText(SolveDrawer.showSolve ? "hide" : "show");
         educationView.onInit();
      });
      ((TextView)findViewById(R.id.hide)).setText(SolveDrawer.showSolve ? "hide" : "show");

      if (SolveDrawer.taskNumber < 2) {
         findViewById(R.id.prev).setEnabled(false);
      }
   }

   private void setTaskDescription () {
      TaskBase taskBase = TaskFactory.getTask(SolveDrawer.taskNumber);
      ((TextView)findViewById(R.id.desc)).setText(SolveDrawer.taskNumber + ". " + taskBase.getDescription());
   }
}
