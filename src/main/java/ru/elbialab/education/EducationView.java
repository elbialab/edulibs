package ru.elbialab.education;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import ru.elbialab.education.solver.SolverHome;
import ru.elbialab.education.tasks.TaskBase;
import ru.elbialab.education.tasks.TaskFactory;

public class EducationView extends View implements View.OnTouchListener {
   private static final float SCALE = 3;
   private float lastTime = System.nanoTime();
   private SolveDrawer solver;
   private TaskBase task;
   private Paint paint = new Paint();
   private Paint solverPaint = new Paint();
   private ViewInfo info = new ViewInfo(0, 0);

   public EducationView(Context context) {
      super(context);
   }

   public EducationView(Context context, AttributeSet attrs) {
      super(context, attrs);
   }

   public EducationView(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
   }

   public void setSolver(SolveDrawer solver) {
      this.solver = solver;
   }

   @Override
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
   }

   @Override
   protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
      super.onLayout(changed, left, top, right, bottom);
      info.width = (right - left)/SCALE;
      info.height = (bottom - top)/SCALE;
      info.left = 10;
      info.top = 10;
      info.context = getContext();

      onInit();
   }

   public void onInit() {
      if (task != null) {
         task.onFinish();
      }
      if (solver == null) {
         solver = new SolverHome();
      }
      task = TaskFactory.getTask(SolveDrawer.taskNumber);
      solver.onInit(info);
      task.onInit(info);
      setOnTouchListener(this);
      invalidate();
   }

   @Override
   protected void onDraw(Canvas canvas) {
      super.onDraw(canvas);

      paint.setAntiAlias(true);
      paint.setStrokeCap(Paint.Cap.ROUND);
      paint.setStrokeWidth(2);
      paint.setStyle(Paint.Style.FILL);

      solverPaint.setAntiAlias(true);
      solverPaint.setStrokeCap(Paint.Cap.ROUND);
      solverPaint.setStrokeWidth(2);
      solverPaint.setStyle(Paint.Style.STROKE);

      canvas.scale(SCALE, SCALE);
      canvas.translate(info.left, info.top);
      drawAxis(canvas, paint, info);

      float currentTime = System.nanoTime();
      float dt = currentTime - lastTime;
      if (currentTime < lastTime) {
         dt = Long.MAX_VALUE - lastTime + currentTime;
      }
      dt /= 10e9f;

      paint.setStyle(Paint.Style.STROKE);
      paint.setColor(0xff000000);
      task.onUpdate(dt, info);
      if (SolveDrawer.showPreview) {
         task.onDraw(canvas, paint, info);
      }

      solverPaint.setColor(0xff000000);
      if (SolveDrawer.showSolve) {
         solver.onUpdate(dt, info);
         solver.onDraw(canvas, solverPaint, info);
      }

      lastTime = currentTime;
      invalidate();
   }

   private void drawAxis(Canvas canvas, Paint paint, ViewInfo info) {
      paint.setStrokeWidth(0.5f);
      paint.setColor(0xffefefef);
      paint.setTextSize(4);
      for (int i = 0; i < Math.max(info.width, info.height); i+=10) {
         paint.setColor(0xffefefef);
         canvas.drawLine(i, -info.top, i, getMeasuredHeight(), paint);
         canvas.drawLine(-info.left, i, getMeasuredWidth(), i, paint);
         paint.setColor(0xff7f7f7f);
         if (i != 0) {
            canvas.drawText(Integer.toString(i), -info.left + 2, i - 1f, paint);
            canvas.drawText(Integer.toString(i), i + 1, -2, paint);
         }
      }

      paint.setStrokeWidth(2f);
      paint.setColor(0xff7f7f7f);
      canvas.drawLine(0, -info.top, 0, getMeasuredHeight(), paint);
      canvas.drawLine(-info.left, 0, getMeasuredWidth(), 0, paint);
   }

   @Override
   public boolean onTouch(View v, MotionEvent event) {
      solver.onTouch((event.getX() - info.left)/SCALE, (event.getY() - info.top)/SCALE, event.getAction());
      task.onTouch((event.getX() - info.left)/SCALE, (event.getY() - info.top)/SCALE, event.getAction());

      return true;
   }
}
