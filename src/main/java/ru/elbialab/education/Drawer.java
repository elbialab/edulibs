package ru.elbialab.education;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Drawer {

   public void onInit(ViewInfo viewInfo) {}

   public void onUpdate(float dt, ViewInfo viewInfo) {}

   public void onDraw(Canvas canvas, Paint paint, ViewInfo viewInfo) {}

   public void onTouch(float x, float y, int motionEvent) {}

   public void onFinish() {}
}
