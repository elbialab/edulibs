package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task004 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисуй круг зеленого цвета с центром (60, 60) и радиусу 40";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafffaf);
      canvas.drawCircle(60, 60, 40, paint);
   }
}
