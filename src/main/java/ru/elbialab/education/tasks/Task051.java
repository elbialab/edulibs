package ru.elbialab.education.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

import ru.elbialab.education.ViewInfo;
import ru.elbialab.javaedu.R;

public class Task051 extends TaskBase {
   private final Random rnd = new Random();
   private boolean run = true;
   Bitmap bmp;

   @Override
   public String getDescription() {
      return "Есть один заказчик и 10 исполнителей. " +
              "Заказчик размещает заказ на сборку какого-ниубдь устройства. " +
              "Сразу после размещения заказ исполнители начинают делать детали для этого устройства. " +
              "После того, как все детали доставлены заказчику, он начинает сборку устройства. " +
              "После выполнения заказа, исполнители ожидают нового заказа. \n" +
              "Реализовать программу, которая с помощью потоков будет имитировать работу такой системы: " +
              "обозначим синим человечком заказчика и черным исполнителя. " +
              "Около ожидающего отображаются часики, у производящего - молоток, доставка - ящик, " +
              "заказ нового устройства - телефон. " +
              "Заказ формируется заказчиком от 1000 до 2000мс. Исполнители создают детали от 1000 до 4000мс. " +
              "Сборка устройства происходит от 1000 до 3000мс. После сборки устройства заказчику необходимо следующее устройство." +
              "Также в квадрате заказчика необходимо показывать номер устройства.";
   }

   private enum State {
      WAIT,
      CALL,
      PRODUCE,
      EXECUTED,
      NONE
   }

   private class Square {
      private final float x;
      private final float y;
      private final float width;
      Thread ownerThread;
      String secondText;
      State state;

      public Square(float x, float y, float width) {
         this.x = x;
         this.y = y;
         this.width = width;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         RectF dst = new RectF(x, y, x + width, y + width);
         Rect src;
         if (this instanceof Executor) {
            src = new Rect(0, 0, 200, 200);
         } else {
            src = new Rect(0, 200, 200, 400);
         }
         canvas.drawBitmap(bmp, src, dst, paint);

         if (state == State.WAIT) {
            src = new Rect(200, 0, 400, 200);
            canvas.drawBitmap(bmp, src, dst, paint);
         } else if (state == State.CALL) {
            src = new Rect(200, 200, 400, 400);
            canvas.drawBitmap(bmp, src, dst, paint);
         } else if (state == State.PRODUCE) {
            src = new Rect(400, 0, 600, 200);
            canvas.drawBitmap(bmp, src, dst, paint);
         } else if (state == State.EXECUTED) {
            src = new Rect(400, 200, 600, 400);
            canvas.drawBitmap(bmp, src, dst, paint);
         }

         if (secondText != null) {
            paint.setTextSize(width/5);
            paint.setColor(0x7f000000);
            paint.setStrokeWidth(1);
            canvas.drawText(secondText, x + 1, y + width * 4 / 5, paint);
         }
      }
   }

   private int executedOrders = 0;
   private final Object NOTIFICATOR = new Object();
   private CountDownLatch processedExecutors;

   private class Customer extends Square implements Runnable {

      public Customer(float x, float y, float width) {
         super(x, y, width);
      }

      @Override
      public void run() {
         ownerThread = Thread.currentThread();
         while (run) {
            generateOrder();
            offer();
            collectOrder();
         }
      }

      private void generateOrder() {
         state = State.CALL;
         executedOrders++;
         secondText = Integer.toString(executedOrders);
         try {
            Thread.sleep(rnd.nextInt(1000) + 1000);
         } catch (InterruptedException e) {
            run = false;
         }
      }

      private void offer() {
         state = State.WAIT;
         try {
            processedExecutors = new CountDownLatch(10);
            synchronized (NOTIFICATOR) {
               NOTIFICATOR.notifyAll();
            }
            processedExecutors.await();
         } catch (InterruptedException e) {
            run = false;
         }
      }

      private void collectOrder() {
         state = State.EXECUTED;
         try {
            Thread.sleep(rnd.nextInt(2000) + 1000);
         } catch (InterruptedException e) {
            run = false;
         }
      }
   }

   private class Executor extends Square implements Runnable {

      public Executor(float x, float y, float width) {
         super(x, y, width);
      }

      @Override
      public void run() {
         ownerThread = Thread.currentThread();
         while (run) {
            waitForOrder();
            produce();
            delivery();
         }
      }

      private void waitForOrder() {
         state = State.WAIT;
         synchronized (NOTIFICATOR) {
            try {
               NOTIFICATOR.wait();
            } catch (InterruptedException e) {
               run = false;
            }
         }
      }

      private void produce() {
         state = State.PRODUCE;
         try {
            Thread.sleep(rnd.nextInt(3000) + 1000);
         } catch (InterruptedException e) {
            run = false;
         }
      }

      private void delivery() {
         state = State.EXECUTED;
         processedExecutors.countDown();
      }
   }

   private final Thread[] threads = new Thread[11];
   private final Square[] sqrs = new Square[11];

   @Override
   public void onInit(ViewInfo viewInfo) {
      // получение Bitmap
      final BitmapFactory.Options options = new BitmapFactory.Options();
      options.inScaled = false;
      options.inPreferredConfig = Bitmap.Config.ARGB_8888;
      bmp = BitmapFactory.decodeResource(
              viewInfo.context.getResources(), R.drawable.cust, options);

      Customer customer;
      sqrs[10] = customer = new Customer(10, 40, 50);
      threads[10] = new Thread(customer);

      for (int i = 0; i < 10; i++) {
         Executor executor = new Executor(70 + i%2*50, 10 + i/2*50, 40);
         sqrs[i] = executor;
         threads[i] = new Thread(executor);
      }

      for (Thread th : threads) {
         th.start();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (Square square : sqrs) {
         square.onDraw(canvas, paint);
      }
   }

   @Override
   public void onFinish() {
      for (Thread th : threads) {
         th.interrupt();
         run = false;
      }
   }
}
