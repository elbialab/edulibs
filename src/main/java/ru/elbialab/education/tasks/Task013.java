package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task013 extends TaskBase {
   @Override
   public String getDescription() {
      return "Прямая должна двигаться вниз-вверх";
   }

   private float t;
   private int sign = 1;
   private float speed = 750;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += speed*sign*dt;
      if (t > viewInfo.height - viewInfo.top - 70) {
         t = viewInfo.height - viewInfo.top - 70;
         sign = -1;
      } else if (t < -30) {
         t = -30;
         sign = 1;
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawLine(40, 30 + t, 120, 70 + t, paint);
   }
}
