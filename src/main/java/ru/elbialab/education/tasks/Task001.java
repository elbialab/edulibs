package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task001 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисуй отрезок с кооринатами (40, 60) и (120, 100)";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawLine(40, 60, 120, 100, paint);
   }
}
