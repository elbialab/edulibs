package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task005 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисуй домик с крышей коричневого цвета и синими стенами";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafcf);
      canvas.drawLine(40, 40, 120, 40, paint);
      canvas.drawLine(120, 40, 120, 120, paint);
      canvas.drawLine(120, 120, 40, 120, paint);
      canvas.drawLine(40, 120, 40, 40, paint);

      paint.setColor(0xffdfbfaf);
      canvas.drawLine(30, 40, 130, 40, paint);
      canvas.drawLine(130, 40, 80, 10, paint);
      canvas.drawLine(80, 10, 30, 40, paint);

      canvas.drawCircle(80, 25, 10, paint);
   }
}
