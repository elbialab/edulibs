package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task012 extends TaskBase {
   @Override
   public String getDescription() {
      return "Не дай прямой улететь за экран";
   }

   private float t;
   private float speed = 750;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += speed*dt;
      if (t > viewInfo.height - viewInfo.top - 70) {
         t = viewInfo.height - viewInfo.top - 70;
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawLine(40, 30 + t, 120, 70 + t, paint);
   }
}
