package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task006 extends TaskBase {
   @Override
   public String getDescription() {
      return "Давай подвигаем прямую (25 точек в секунду)";
   }

   private float t;
   private float speed = 250;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += speed*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawLine(40, 30 + t, 120, 70 + t, paint);
   }
}
