package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task022 extends TaskBase {
   @Override
   public String getDescription() {
      return "Три соединенные точки двигаются туда-сюда";
   }

   private class Point {
      float x;
      float y;

      public Point(float x, float y) {
         this.x = x;
         this.y = y;
      }
   }

   private class Entity {
      Point p0;
      Point p;
      Point s;
      boolean reverse;
      float t;
      float speed;
   }

   private Random rnd = new Random();
   private Entity[] entities = new Entity[3];

   @Override
   public void onInit(ViewInfo viewInfo) {
      for (int i = 0; i < entities.length; i++) {
         Entity entity = new Entity();
         entity.s = new Point(rnd.nextFloat()*100 - 50, rnd.nextFloat()*100 - 50);
         entity.p0 = new Point(viewInfo.width/2 + rnd.nextFloat()*50 - 25,
                 viewInfo.height/2 + rnd.nextFloat()*50 - 25);
         entity.reverse = false;
         entity.speed = rnd.nextFloat()*2;
         entity.t = 0;
         entities[i] = entity;
      }
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      super.onUpdate(dt, viewInfo);
      for (int i = 0; i < entities.length; i++) {
         Entity e = entities[i];
         e.t += (e.reverse ? -1 : 1)*dt*e.speed;
         if (e.t > 1) {
            e.reverse = true;
            e.t = 1;
         } else if (e.t < 0) {
            e.t = 0;
            e.reverse = false;
         }

         e.p = new Point(e.p0.x + e.t * e.s.x, e.p0.y + e.t*e.s.y);
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (int i = 0; i < entities.length; i++) {
         paint.setColor(0x3f7f7f7f);
         Entity e0 = entities[i];
         Entity e1 = entities[(i + 1) % 3];
         canvas.drawLine(e0.p.x, e0.p.y, e1.p.x, e1.p.y, paint);
      }

      for (int i = 0; i < entities.length; i++) {
         paint.setColor(0x3f0000ff);
         Entity e = entities[i];
         canvas.drawCircle(e.p.x, e.p.y, 2, paint);
      }
   }
}
