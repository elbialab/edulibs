package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import ru.elbialab.education.ViewInfo;

public class Task041 extends TaskBase {
   private Path bag;

   @Override
   public String getDescription() {
      return "Игра: В вазе лежат шарики разных цветов. Игрок не знает какие. " +
              "Ему дается панель с цветами и он должен найти перебором шарики какого цвета лежат в вазе.";
   }

   private Set<Integer> bagSet = new HashSet<>();
   private Set<Integer> foundSet = new HashSet<>();
   private Random rnd = new Random();
   private int[][] colors = new int[10][10];
   private List<Cell> cells = new ArrayList<>();
   private FindBall findBall;

   private class FindBall {
      private int color;
      private float x;
      private float y;
      private float radius;
      private float t;

      public FindBall(int color) {
         this.color = color;
         this.x = 50;
         this.y = 90;
         this.radius = 10;
      }

      public void onUpdate(float dt) {
         t += dt*5;
         int alpha = Math.min(255, Math.round(255*(1 - t)));
         color = color & ((alpha << 24) | (color & 0x00ffffff));
         y = 90*(1 - t);
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawCircle(x, y, radius,  paint);
      }
   }

   private class Cell {
      private int color;
      private int x;
      private int y;
      private int width;
      private int height;
      private boolean selected;

      public Cell(int color, float x, float y, float width, float height) {
         this.color = color;
         this.x = (int) x;
         this.y = (int) y;
         this.width = (int) width;
         this.height = (int) height;
      }

      public boolean collapse(float x, float y) {
         return this.x <= x && x <= this.x + width && this.y <= y && y <= this.y + height;
      }

      public void setSelected() {
         selected = true;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawRect(x, y, x + width, y + height, paint);

         if (selected) {
            paint.setColor(0x3f000000);
            canvas.drawCircle(x + width/2f, y + height/2f, Math.min(width, height)/2f, paint);
         }

         if (foundSet.contains(color)) {
            paint.setColor(0x3f00ff00);
            canvas.drawCircle(x + width/2f, y + height/2f, Math.min(width, height)/3f, paint);
         }
      }
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      bag  = new Path();
      bag.moveTo(20, 20);
      bag.lineTo(30, 30);
      bag.lineTo(10, 50);
      bag.lineTo(10, 80);
      bag.lineTo(30, 100);
      bag.lineTo(70, 100);
      bag.lineTo(90, 80);
      bag.lineTo(90, 50);
      bag.lineTo(70, 30);
      bag.lineTo(80, 20);

      for (int i = 0; i < colors.length; i++) {
         for (int j = 0; j < colors[i].length; j++) {
            colors[i][j] = rnd.nextInt() | 0x7f000000;
            float w = viewInfo.width/colors.length;
            float h = (viewInfo.height - 110)/colors[0].length*2/3;
            Cell cell = new Cell(colors[i][j],
                    i * w - viewInfo.left, j * h - viewInfo.top + 120,
                    w, h);
            cells.add(cell);
         }
      }

      int bagCount = rnd.nextInt(colors.length*colors[0].length);
      for (int i = 0; i < bagCount; i++) {
         int x = rnd.nextInt(colors.length);
         int y = rnd.nextInt(colors[0].length);
         bagSet.add(colors[x][y]);
      }
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      if (findBall != null) {
         findBall.onUpdate(dt);
         if (findBall.t > 1) {
            findBall = null;
         }
      }
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (motionEvent == MotionEvent.ACTION_UP) {
         for (Cell cell : cells) {
            if (cell.collapse(x, y)) {
               cell.setSelected();
               if (bagSet.contains(cell.color)) {
                  foundSet.add(cell.color);
                  findBall = new FindBall(cell.color);
               }
            }
         }
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawPath(bag, paint);

      for (Cell cell : cells) {
         cell.onDraw(canvas, paint);
      }

      if (findBall != null) {
         findBall.onDraw(canvas, paint);
      }
   }
}
