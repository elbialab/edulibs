package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task008 extends TaskBase {
   @Override
   public String getDescription() {
      return "Точка (30, 50) движется через точку (100, 20)";
   }

   private Point p = new Point();
   private Vector dir = new Vector();

   @Override
   public void onInit(ViewInfo viewInfo) {
      p.x = 30;
      p.y = 50;

      dir.x = 100 - 30;
      dir.y = 20 - 50;
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      p.x += dir.x*dt;
      p.y += dir.y*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawCircle(30, 50, 2, paint);
      canvas.drawCircle(100, 20, 2, paint);

      paint.setColor(0xff007f00);
      canvas.drawCircle(p.x, p.y, 2, paint);
   }
}
