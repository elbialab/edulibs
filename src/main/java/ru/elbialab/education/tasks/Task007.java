package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task007 extends TaskBase {
   @Override
   public String getDescription() {
      return "Движение прямой в направлении (3, 5). Прямая с координатами (40, 30) (120, 70). Коээфициент скорости 25.";
   }

   private float t;
   private float speed = 25;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += speed*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawLine(40 + 3*t, 30 + 5*t, 120 + 3*t, 70 + 5*t, paint);
   }
}
