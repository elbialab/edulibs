package ru.elbialab.education.tasks;

import java.util.HashMap;
import java.util.Map;

public class TaskFactory {
   private static Map<Integer, TaskBase> tasks = new HashMap<>();

   static {
      registerTask(new Task000());
   }

   private static void registerTask(TaskBase task) {
      tasks.put(task.getTaskNumber(), task);
   }

   public static TaskBase getTask(int taskNumber) {
      TaskBase taskBase = tasks.get(taskNumber);
      if (taskBase == null) {
         taskBase = tryLoadTask(taskNumber);
         if (taskBase == null) {
            return tasks.get(0);
         }
      }

      return taskBase;
   }

   private static TaskBase tryLoadTask(int taskNumber) {
      StringBuilder sb = new StringBuilder(TaskBase.class.getPackage().getName());
      sb.append(".Task");

      if (taskNumber < 10) {
         sb.append("0");
      }

      if (taskNumber < 100) {
         sb.append("0");
      }
      sb.append(taskNumber);

      try {
         Class<TaskBase> taskBaseClass = (Class<TaskBase>) TaskBase.class.getClassLoader().loadClass(sb.toString());
         TaskBase task = taskBaseClass.newInstance();
         return task;
      } catch (Exception e) {
         return null;
      }
   }
}
