package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

import ru.elbialab.education.ViewInfo;

public class Task046 extends TaskBase {
   private List<Square> sqs = new ArrayList<>();
   private Thread initThread;

   @Override
   public String getDescription() {
      return "Создать 40 потоков. Каждый поток отображать квадратом:\n" +
              "зеленого цвета - поток завершился;\n" +
              "желтого цвета - поток выполняется;\n" +
              "красного цвета - поток ожидает выполнения;\n" +
              "серого цвета - поток еще не запущен.\n" +
              "Первые 20 потоков стартуют с интервалом в 100мс, остальные стартуют сразу же после 20-го. Все потоки пытаются зайти в критическую секцию, " +
              "задача в этой секции выполнятеся 250мс";
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      initThread = new Thread(() -> {
         for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 4; y++) {
               Square s = new Square(x * 30, y * 30, 30, new Thread(this::criticalSection));
               sqs.add(s);
            }
         }

         int i = 0;
         for (Square sq : sqs) {
            sq.thread.start();
            i++;
            if (i <= 20) {
               try {
                  Thread.sleep(100);
               } catch (InterruptedException e) {
                  e.printStackTrace();
               }
            }
         }
      });
      initThread.setUncaughtExceptionHandler((t, e) -> {
         e.printStackTrace();
      });
      initThread.start();
   }

   private synchronized void criticalSection() {
      try {
         Thread.sleep(250);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (Square s : sqs) {
         s.draw(canvas, paint);
      }
   }

   private class Square {
      private float x;
      private float y;
      private float width;
      private Thread thread;

      public Square(float x, float y, float width, Thread thread) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.thread = thread;
      }

      public void draw(Canvas canvas, Paint paint) {
         paint.setStyle(Paint.Style.FILL);
         switch (thread.getState()) {
            case NEW:
               paint.setColor(0x7f7f7f7f);
               break;
            case RUNNABLE:
            case TIMED_WAITING:
               paint.setColor(0x7fffff00);
               break;
            case WAITING:
            case BLOCKED:
               paint.setColor(0x7fff0000);
               break;
            case TERMINATED:
               paint.setColor(0x7f00ff00);
               break;
         }
         canvas.drawRect(x, y, x + width, y + width, paint);
      }
   }
}
