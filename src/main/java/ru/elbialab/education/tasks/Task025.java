package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import ru.elbialab.education.ViewInfo;

public class Task025 extends TaskBase {
   @Override
   public String getDescription() {
      return "Цвет зеленого квадрата меняется на красный при нажатии на квадрат";
   }

   private int color = 0x7f00ff00;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(color);
      paint.setStyle(Paint.Style.FILL_AND_STROKE);
      canvas.drawRect(20, 20, 120, 120, paint);
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (motionEvent == MotionEvent.ACTION_DOWN) {
         if (20 <= x && x <= 120 && 20 <= y && y <= 120) {
            color = 0x7fff0000;
         }
      } else if (motionEvent == MotionEvent.ACTION_UP) {
         color = 0x7f00ff00;
      }
   }
}
