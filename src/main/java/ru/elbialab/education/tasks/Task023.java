package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task023 extends TaskBase {
   @Override
   public String getDescription() {
      return "Радиус окружности меняется в зависимости от расстояния";
   }

   private class Point {
      float x;
      float y;

      public Point(float x, float y) {
         this.x = x;
         this.y = y;
      }
   }

   private Random rnd = new Random();
   private float t;
   private Point p0;
   private Point speed;
   private Point p;
   private float r;

   @Override
   public void onInit(ViewInfo viewInfo) {
      speed = new Point(rnd.nextFloat()*200 - 100, rnd.nextFloat()*200 - 100);
      p0 = new Point(viewInfo.width*0.25f, viewInfo.height*0.25f);
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      super.onUpdate(dt, viewInfo);
      t += dt;
      p = new Point(p0.x + t*speed.x, p0.y + t*speed.y);
      r = (float) Math.sqrt((p0.x - p.x)*(p0.x - p.x) + (p0.y - p.y)*(p0.y - p.y));
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0x3f00ff00);
      canvas.drawCircle(p0.x, p0.y, 2, paint);
      paint.setColor(0x3fff0000);
      canvas.drawCircle(p.x, p.y, r, paint);
   }
}
