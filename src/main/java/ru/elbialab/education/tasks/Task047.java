package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ru.elbialab.education.ViewInfo;

public class Task047 extends TaskBase {
   private Random rnd = new Random();
   private List<Square> sqs = new ArrayList<>();
   private Lock lock = new ReentrantLock();

   @Override
   public String getDescription() {
      return "Создать 40 потоков. Каждый поток отображать квадратом:\n" +
              "зеленого цвета - поток завершился;\n" +
              "желтого цвета - поток выполняется;\n" +
              "красного цвета - поток ожидает выполнения;\n" +
              "серого цвета - поток еще не запущен.\n" +
              "Потоки стартуют все сразу. Есть две критические секции. Случайная половина выполняет первую секцию," +
              " а вторая часть - вторую. Первая критическая секция выполняется 250мс, вторая - 400мс. " +
              "Причем, если одна из критических секций занята, вторая также блокируется";
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      for (int x = 0; x < 10; x++) {
         for (int y = 0; y < 4; y++) {
            Square s = new Square(x * 30, y * 30, 30, new Thread(
                    rnd.nextBoolean() ? this::criticalSection1 : this::criticalSection2));
            sqs.add(s);
         }
      }

      for (Square sq : sqs) {
         sq.thread.start();
      }
   }

   private void criticalSection1() {
      lock.lock();
      try {
         Thread.sleep(250);
      } catch (InterruptedException e) {
         e.printStackTrace();
      } finally {
         lock.unlock();
      }
   }

   private void criticalSection2() {
      lock.lock();
      try {
         Thread.sleep(400);
      } catch (InterruptedException e) {
         e.printStackTrace();
      } finally {
         lock.unlock();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (Square s : sqs) {
         s.draw(canvas, paint);
      }
   }

   private class Square {
      private float x;
      private float y;
      private float width;
      private Thread thread;

      public Square(float x, float y, float width, Thread thread) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.thread = thread;
      }

      public void draw(Canvas canvas, Paint paint) {
         paint.setStyle(Paint.Style.FILL);
         switch (thread.getState()) {
            case RUNNABLE:
            case TIMED_WAITING:
               paint.setColor(0x7fffff00);
               break;
            case WAITING:
            case BLOCKED:
               paint.setColor(0x7fff0000);
               break;
            case TERMINATED:
               paint.setColor(0x7f00ff00);
               break;
            default:
               paint.setColor(0x7f7f7f7f);
               break;
         }
         canvas.drawRect(x, y, x + width, y + width, paint);
      }
   }
}
