package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task010 extends TaskBase {
   @Override
   public String getDescription() {
      return "Точка (100, 150) двигается со скоростью (30, 40) и ускорением (-20, -40)";
   }

   private Point p = new Point();
   private Vector speed = new Vector();
   private Vector a = new Vector();

   @Override
   public void onInit(ViewInfo viewInfo) {
      p.x = 100; p.y = 150;
      speed.x = 30;
      speed.y = 40;
      a.x = -20;
      a.y = -40;
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      speed.x += 2*a.x*dt;
      speed.y += 2*a.y*dt;

      p.x += speed.x*dt;
      p.y += speed.y*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawCircle(100, 150, 2, paint);

      paint.setColor(0xff007f00);
      canvas.drawCircle(p.x, p.y, 2, paint);
   }
}
