package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

import ru.elbialab.education.ViewInfo;

public class Task045 extends TaskBase {
   @Override
   public String getDescription() {
      return "100 квадратов появляются раз в 250 миллисекунд. После того как все квадраты появились, сообщить об этом";
   }

   private class Square {
      private float x;
      private float y;
      private float width;

      public Square(float x, float y, float width) {
         this.x = x;
         this.y = y;
         this.width = width;
      }

      public void draw(Canvas canvas, Paint paint) {
         canvas.drawRect(x, y, x + width, y + width, paint);
      }
   }

   private List<Square> sqs = new ArrayList<>();
   private Thread initThread;

   @Override
   public void onInit(ViewInfo viewInfo) {
      initThread = new Thread(() -> {
         for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
               Square s = new Square(x*30, y*30, 30);
               sqs.add(s);
               try {
                  Thread.sleep(250);
               } catch (InterruptedException e) {
                  e.printStackTrace();
               }
            }
         }
      });
      initThread.setUncaughtExceptionHandler((t, e) -> {
         e.printStackTrace();
      });
      initThread.start();
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0x7f000000);
      for (Square s : sqs) {
         s.draw(canvas, paint);
      }

      if (initThread.getState() == Thread.State.TERMINATED) {
         paint.setTextSize(15);
         canvas.drawText("OK", 10, 10, paint);
      }
   }
}
