package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import ru.elbialab.education.ViewInfo;

public class Task028 extends TaskBase {
   @Override
   public String getDescription() {
      return "Аналоговые часы";
   }

   private Hand hourHand;
   private Hand minuteHand;
   private Hand secondHand;

   class Hand {
      float cx;
      float cy;
      float radius;
      float weight;
      float maxValue;
      float realValue;
      int color;

      Hand(float cx, float cy, float radius, float weight, long maxValue) {
         this.cx = cx;
         this.cy = cy;
         this.radius = radius;
         this.weight = weight;
         this.maxValue = maxValue;
         realValue = 0;
         color = 0x7f000000;
      }

      void setRealValue(long realValue) {
         this.realValue = realValue;
      }

      void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStrokeWidth(weight);
         float t = realValue/maxValue;
         canvas.drawLine(cx, cy,
                 (float)(cx + radius*Math.cos(3*Math.PI/2 + 2 * t * Math.PI)),
                 (float)(cy + radius*Math.sin(3*Math.PI/2 + 2 * t * Math.PI)),
                 paint);
      }
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      float cx = viewInfo.width/2 - viewInfo.left;
      float cy = viewInfo.height/2 - viewInfo.top;
      float radius = Math.min(cx, cy)/1.5f;
      hourHand = new Hand(cx, cy, radius, 6, (int) TimeUnit.HOURS.toSeconds(12));
      minuteHand = new Hand(cx, cy, radius*1.2f, 3, (int) TimeUnit.MINUTES.toSeconds(60));
      secondHand = new Hand(cx, cy, radius*1.2f, 1, 60);
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      Calendar cal = Calendar.getInstance();
      int second = cal.get(Calendar.SECOND);
      int minute = cal.get(Calendar.MINUTE);
      int hour = cal.get(Calendar.HOUR_OF_DAY);

      hourHand.setRealValue(TimeUnit.MINUTES.toSeconds(TimeUnit.HOURS.toMinutes(hour) + minute) + second);
      minuteHand.setRealValue(TimeUnit.MINUTES.toSeconds(minute) + second);
      secondHand.setRealValue(second);
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0x7f000000);
      paint.setStrokeWidth(2);
      for (int i = 0; i < 60; i++) {
         canvas.drawCircle((float)(hourHand.cx + hourHand.radius*1.3f * Math.cos(3*Math.PI/2 + 2 * i/60.0 * Math.PI)),
                 (float)(hourHand.cy + hourHand.radius*1.3f * Math.sin(3*Math.PI/2 + 2 * i/60.0 * Math.PI)),
                 i % 5 == 0 ? 2 : 1, paint);
      }

      hourHand.onDraw(canvas, paint);
      minuteHand.onDraw(canvas, paint);
      secondHand.onDraw(canvas, paint);
   }
}
