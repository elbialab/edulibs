package ru.elbialab.education.tasks;

/**
 * @author user
 * @date 13.07.2019
 */

public class Point {
   public float x;
   public float y;

   public Point() {
   }

   public Point(float x, float y) {
      this.x = x;
      this.y = y;
   }

   public Point(double x, double y) {
      this.x = (float)x;
      this.y = (float)y;
   }
}
