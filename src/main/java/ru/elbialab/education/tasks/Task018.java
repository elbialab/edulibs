package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task018 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисовать переход от красного к зеленому цвету";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (int i = 0; i < 255; i++) {
         paint.setColor(0x3fffffff & Color.rgb(255 - i, i, 0));
         canvas.drawLine(-viewInfo.left, i, viewInfo.width - viewInfo.left, i, paint);
      }
   }
}
