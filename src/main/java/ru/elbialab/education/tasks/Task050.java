package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Semaphore;

import ru.elbialab.education.ViewInfo;

public class Task050 extends TaskBase {
   private final Random rnd = new Random();
   private boolean run = true;
   private final Object WAITER = new Object();
   private final List<Train> trains = new ArrayList<>();
   private PathPart path;
   private float dt;

   private class PathPart {
      Semaphore semaphore;
      PathPart next;
      private Point semaphorePos;
      List<Path> list = new ArrayList<>();
      final Set<Path> free = new HashSet<>();

      public void addPath(Path path) {
         free.add(path);
         list.add(path);
      }

      public void init() {
         semaphore = new Semaphore(free.size());
      }

      public Path acquirePath() throws InterruptedException {
         semaphore.acquire();
         synchronized (free) {
            Iterator<Path> i = free.iterator();
            Path path = i.next();
            i.remove();
            return path;
         }
      }

      public void releasePath(Path path) {
         synchronized (free) {
            free.add(path);
            semaphore.release();
         }
      }

      public void onDraw(Canvas canvas, Paint paint) {
         for (Path p : list) {
            p.onDraw(canvas, paint);
         }

         paint.setStyle(Paint.Style.FILL);
         paint.setColor(0x7f000000);
         canvas.drawRect(semaphorePos.x - 7, semaphorePos.y - 14,
                 semaphorePos.x + 7, semaphorePos.y + 14, paint);
         if (semaphore.availablePermits() == 0) {
            paint.setColor(0x7fff0000);
            canvas.drawCircle(semaphorePos.x, semaphorePos.y - 7, 6, paint);
         } else {
            paint.setColor(0x7f00ff00);
            canvas.drawCircle(semaphorePos.x, semaphorePos.y + 7, 6, paint);
         }

         if (next != path) {
            next.onDraw(canvas, paint);
         }
      }

      public boolean near(Point p) {
         Point s = list.get(0).s;
         return (Math.hypot(s.x - p.x, s.y - p.y)) <= 15;
      }
   }

   private static class Path {
      Point s;
      Point f;
      float len;
      Path next;

      public Path(Point s, Point f) {
         this.s = s;
         this.f = f;
         len = (float) Math.hypot(s.x - f.x, s.y - f.y);
      }

      Point pointAtPath(float dLen) {
         if (dLen < 0) {
            return null;
         } else if (next == null && dLen >= len) {
            return null;
         } else if (next != null && dLen >= len) {
            return next.pointAtPath(dLen - len);
         }

         float t = dLen / len;
         return new Point(s.x + t * (f.x  - s.x), s.y + t*(f.y - s.y));
      }

      void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(0x7f000000);
         canvas.drawLine(s.x, s.y, f.x, f.y, paint);
         if (next != null) {
            next.onDraw(canvas, paint);
         }
      }
   }

   private static class Point {
      float x;
      float y;

      public Point(double x, double y) {
         this.x = (float) x;
         this.y = (float) y;
      }

      public Point(float x, float y) {
         this.x = x;
         this.y = y;
      }
   }

   private class Train extends Thread {
      private Point pos;
      private int color;
      private PathPart currentPart;
      private PathPart nextPart;
      private Path currentPath;
      private Path nextPath;
      private float speed;
      private float maxSpeed;
      private float p = 0;
      private boolean semaphoreAcquired = false;

      @Override
      public void run() {
         setUncaughtExceptionHandler((t, e) -> e.printStackTrace());
         maxSpeed = speed;
         while (run) {
            try {
               waitForUpdate();
               acquireNextPart();
               changeSpeed();
               move();
            } catch (InterruptedException e) {
               e.printStackTrace();
               return;
            }
         }
      }

      void waitForUpdate() throws InterruptedException {
         synchronized (WAITER) {
            WAITER.wait();
         }
      }

      void acquireNextPart() throws InterruptedException {
         if (nextPart == null) {
            currentPath = currentPart.acquirePath();
            nextPart = currentPart.next;
            semaphoreAcquired = true;
         } else if (nextPart.near(pos) && !semaphoreAcquired) {
            currentPart.releasePath(currentPath);
            nextPath = nextPart.acquirePath();
            semaphoreAcquired = true;
         } else if (!nextPart.near(pos)){
            semaphoreAcquired = false;
         }
      }

      void changeSpeed() {
         if (nextPart.semaphore.hasQueuedThreads()) {
            speed -= 3;
            if (speed < 0) {
               speed = 0;
            }
         } else {
            speed += 3;
            if (speed > maxSpeed) {
               speed = maxSpeed;
            }
         }
      }

      void move() {
         p += dt*speed;
         Point newPos = currentPath.pointAtPath(p);
         if (newPos == null) {
            currentPart = nextPart;
            nextPart = nextPart.next;
            currentPath = nextPath;
            p = 0;
            semaphoreAcquired = false;
         } else {
            pos = newPos;
         }
      }

      void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawCircle(pos.x, pos.y, 6, paint);
      }
   }

   @Override
   public String getDescription() {
      return "Реализовать движение точек по ломаной линий со случайной скоростью от 50 до 250. " +
              "Если между двумя узлами заняты все участки прямых, то точка должна ждать разрешения продолжить путь.";
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      Point c = new Point(viewInfo.width/2 - viewInfo.left, viewInfo.height/2 - viewInfo.top);
      float r = Math.min(viewInfo.width, viewInfo.height)*0.4f;

      PathPart start = null;
      for (int i = 0; i < 7; i++) {
         Point s = new Point(c.x + r*Math.cos(2*Math.PI/7*i), c.y + r*Math.sin(2*Math.PI/7*i));
         Point f = new Point(c.x + r*Math.cos(2*Math.PI/7*(i + 1)), c.y + r*Math.sin(2*Math.PI/7*(i + 1)));
         PathPart next = new PathPart();
         if (path == null) {
            path = next;
            start = next;
         } else {
            path.next = next;
            path = next;
         }
         Path p = new Path(s, f);
         path.semaphorePos = new Point(c.x + (r*1.15)*Math.cos(2*Math.PI/7*i), c.y + (r*1.15)*Math.sin(2*Math.PI/7*i));
         path.addPath(p);
         for (int j = -rnd.nextInt(4); j < rnd.nextInt(4) + 1; j++) {
            if (j == 1) {
               continue;
            }
            Point cc = new Point(c.x + r/10*(10 - j)*Math.cos(2*Math.PI/7*(i + 0.5)),
                    c.y + r/10*(10 - j)*Math.sin(2*Math.PI/7*(i + 0.5)));
            Path p1 = new Path(s, cc);
            p1.next = new Path(cc, f);
            path.addPath(p1);
            addTrain(path, p1);
         }
         path.init();
      }
      path.next = start;
   }

   private void addTrain(PathPart part, Path p) {
      Train t = new Train();
      t.p = 0;
      t.color = (rnd.nextInt() | 0x7f000000) & 0x7fffffff;
      t.pos = p.s;
      t.currentPart = part;
      t.speed = 200 + rnd.nextFloat()*200;
      t.semaphoreAcquired = true;
      t.start();
      trains.add(t);
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      this.dt = dt;
      synchronized (WAITER) {
         WAITER.notifyAll();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      path.onDraw(canvas, paint);
      for (Train train : trains) {
         train.onDraw(canvas, paint);
      }
   }

   @Override
   public void onFinish() {
      run = false;
   }
}
