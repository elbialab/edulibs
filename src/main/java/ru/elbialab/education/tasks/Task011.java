package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task011 extends TaskBase {
   @Override
   public String getDescription() {
      return "Отобразить точку пересечения двух прямых";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      float x01 = 40;
      float y01 = 30;
      float x02 = 120;
      float y02 = 70;

      float x11 = 50;
      float y11 = 60;
      float x12 = 140;
      float y12 = 10;

      /*
              y0 = kx0 + b
              y1 = kx1 + b

              b = y0 - kx0
              y1 = kx1 + y0 - kx0

              y1 - y0 = k(x1 - x0)
              k = (y1 - y0)/(x1 - x0)

               */

      float k1 = (y02 - y01)/(x02 - x01);
      float b1 = y01 - k1*x01;

      float k2 = (y12 - y11)/(x12 - x11);
      float b2 = y11 - k2*x11;

      /*
            y = k1 x + b1
            y = k2 x + b2

            k1 x + b1 = k2 x + b2
            b2 - b1 = x(k1 - k2)
            x = (b2 - b1)/(k1 - k2)

       */

      float cx = (b2 - b1)/(k1 - k2);
      float cy = k1 * cx + b1;

      paint.setColor(0xffafafaf);
      canvas.drawLine(x01, y01, x02, y02, paint);
      canvas.drawLine(x11, y11, x12, y12, paint);

      paint.setColor(0xff7f0000);
      canvas.drawCircle(cx, cy, 2, paint);
   }
}
