package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import ru.elbialab.education.ViewInfo;

public class Task035 extends TaskBase {
   @Override
   public String getDescription() {
      return "Дан круг. На каждом шаге рисуется один круг снаружи и два внутри.";
   }

   Point c = new Point(100, 100);
   float r = 75;
   float t = 0;
   float angle = (float) (Math.PI/2);
   final float DR = 2.3f;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t = t + 10*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      int iteration = Math.round(t);
      paint.setColor(0xff7f7f7f);
      process(iteration, canvas, paint, c, r, angle);
   }

   private Point calcCenter(Point p, float r, float angle, int pos) {
      angle += pos*2*Math.PI/3;

      float nr = pos == 0 ? r + r/DR : r - r/DR;
      return new Point(p.x + nr * Math.cos(angle), p.y + nr * Math.sin(angle));
   }

   private void process(int iteration, Canvas canvas, Paint paint, Point c, float r, float angle) {
      canvas.drawCircle(c.x, c.y, r, paint);
      if (iteration <= 0) {
         return;
      }

      process(iteration - 1, canvas, paint, calcCenter(c, r, angle, 0), r/DR, angle);
      process(iteration - 1, canvas, paint, calcCenter(c, r, angle, 1), r/DR, (float) (angle - Math.PI/6));
      process(iteration - 1, canvas, paint, calcCenter(c, r, angle, 2), r/DR, (float) (angle + Math.PI/6));
   }
}
