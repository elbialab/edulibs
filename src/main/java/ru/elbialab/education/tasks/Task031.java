package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task031 extends TaskBase {
   @Override
   public String getDescription() {
      return "Текст \"Hello world!\" должен меняться согласно образцу";
   }

   private float t = 0;
   private String s = "Hello world!";
   private int lastPos = 0;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += 20*dt;
      int pos = Math.round(t) % s.length();
      if (pos != lastPos) {
         s = s.substring(1) + s.charAt(0);
         lastPos = pos;
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xff7f7f7f);
      paint.setStyle(Paint.Style.FILL);
      paint.setTextSize(10);
      canvas.drawText(s, 10, 10, paint);
   }
}
