package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task019 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисовать 300 точек в случайном месте";
   }

   private Random rnd = new Random();

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0x3fff0000);
      for (int i = 0; i < 300; i++) {
         canvas.drawCircle(rnd.nextFloat()*viewInfo.width - viewInfo.left,
                 rnd.nextFloat()*viewInfo.height - viewInfo.top, 1, paint);
      }
   }
}
