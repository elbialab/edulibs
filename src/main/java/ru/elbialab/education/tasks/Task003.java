package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task003 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисуй круг с центром (60, 60) и радиусу 40";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawCircle(60, 60, 40, paint);
   }
}
