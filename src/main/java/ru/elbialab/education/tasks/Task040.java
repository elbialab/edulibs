package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import ru.elbialab.education.ViewInfo;

public class Task040 extends TaskBase {
   private Point p = new Point(100, 100);
   private Point move = new Point(0, 75);
   private float r = 75;
   private float speed = 0;
   private float a = -1;
   private float angle = 0;
   private float t = 0;
   private float lx, ly;
   private boolean run = false;
   float lastAngle = 0;

   @Override
   public String getDescription() {
      return "Раскрутить шарик";
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      if (run) {
         float lastSpeed = speed;
         speed += a * dt / 2;
         angle += speed * dt;
         if (lastSpeed * speed <= 0) {
            speed = 0;
            run = false;
         }
      }

      float x = 0;
      float y = r;
      move = new Point(
              (float) (x * Math.cos(angle) - y * Math.sin(angle)),
              (float) (x * Math.sin(angle) + y * Math.cos(angle))
      );
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL);
      paint.setColor(0x7f000000);
      canvas.drawLine(p.x, p.y, p.x + move.x, p.y + move.y, paint);
      canvas.drawCircle(p.x, p.y, 5, paint);
      paint.setColor(0x7f00ff00);
      canvas.drawCircle(p.x + move.x, p.y + move.y, 20, paint);
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (MotionEvent.ACTION_DOWN == motionEvent) {
         lx = x;
         ly = y;
         lastAngle = angle;
      } else if (MotionEvent.ACTION_MOVE == motionEvent) {
         float vx0 = lx - p.x;
         float vy0 = ly - p.y;
         float vx1 = x - p.x;
         float vy1 = y - p.y;
         float cos = vx0 * vx1 + vy0 * vy1;
         float sin = vx0 * vy1 - vx1 * vy0;
         angle = (float) Math.atan2(sin, cos) + lastAngle;

         while (angle > Math.PI) {
            angle -= 2*Math.PI;
         }

         while (angle < -Math.PI) {
            angle += 2*Math.PI;
         }

         speed = 10*angle;
         if (speed > 0) {
            a = -20f;
         } else if (speed < 0) {
            a = 20f;
         }
      } else if (MotionEvent.ACTION_UP == motionEvent) {
         run = true;
      }
   }
}
