package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task033 extends TaskBase {
   @Override
   public String getDescription() {
      return "Даны три точки. На каждом шаге находится точка посередине отрезка между текущей точкой и одной из данных точек";
   }

   Point[] src = {new Point(30, 40), new Point(150, 60), new Point(80, 150)};
   List<Point> points = new ArrayList<>();
   Point current;
   Random rnd = new Random();

   float t = 0;


   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t = t + 400*dt;
      if (Math.round(t) > points.size()) {
         Point a = src[rnd.nextInt(3)];
         Point b = current == null ? new Point(rnd.nextInt(200), rnd.nextInt(200)) : current;
         current = new Point((a.x + b.x)/2, (a.y + b.y)/2);
         points.add(current);
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xff007f00);
      for (Point p : src) {
         canvas.drawCircle(p.x, p.y, 1, paint);
      }
      paint.setColor(0xff7f7f7f);
      for (Point p : points) {
         canvas.drawCircle(p.x, p.y, 1, paint);
      }
   }
}
