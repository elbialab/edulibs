package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task016 extends TaskBase {
   @Override
   public String getDescription() {
      return "Движение кружка по направлению (2, 3) с отскоком от границ прямоугольника";
   }

   private float speed = 200;

   private float dx = 2;
   private float dy = 3;
   private float xsign = 1;
   private float ysign = 1;

   private float x = 30;
   private float y = 30;

   private float left = 10;
   private float right = 100;
   private float top = 10;
   private float bottom = 200;

   private float r = 10;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      x += dt*speed*xsign*dx;
      y += dt*speed*ysign*dy;

      if (x + r > right) {
         x = right - r;
         xsign = -1;
      } else if (x - r < left) {
         x = left + r;
         xsign = 1;
      }

      if (y + r > bottom) {
         y = bottom - r;
         ysign = -1;
      } else if (y - r < top) {
         y = top + r;
         ysign = 1;
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafdfdf);
      canvas.drawRect(left, top, right, bottom, paint);

      paint.setColor(0xffdfdfaf);
      canvas.drawCircle(x, y, r, paint);
   }
}
