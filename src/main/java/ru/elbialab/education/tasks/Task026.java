package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import ru.elbialab.education.ViewInfo;

public class Task026 extends TaskBase {
   @Override
   public String getDescription() {
      return "Замостить экран квадратами, на каждый из которых можно нажать";
   }

   private Square[] squares;
   private Square pressedSquare;

   class Square {
      float x;
      float y;
      float width;
      float height;
      boolean pressed = false;

      public Square(float x, float y, float width, float height) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.height = height;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(pressed ? 0x7fff0000 : 0x7f00ff00);
         canvas.drawRect(x, y, x + width, y + height, paint);
      }

      public boolean collapse(float x, float y) {
         return this.x <= x && x <= this.x + width && this.y <= y && y <= this.y + height;
      }
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      squares = new Square[9];
      for (int i = 0; i < squares.length; i++) {
         Square square = new Square(i % 3 * 60 + 10, i / 3 * 60 + 10, 50, 50);
         squares[i] = square;
      }
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL_AND_STROKE);
      for (Square square : squares) {
         square.onDraw(canvas, paint);
      }
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (motionEvent == MotionEvent.ACTION_DOWN) {
         for (Square square : squares) {
            if (square.collapse(x, y)) {
               square.pressed = true;
               pressedSquare = square;
            }
         }
      } else if (motionEvent == MotionEvent.ACTION_UP) {
         if (pressedSquare != null) {
            pressedSquare.pressed = false;
            pressedSquare = null;
         }
      }
   }
}
