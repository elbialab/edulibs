package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

import ru.elbialab.education.ViewInfo;

public class Task032 extends TaskBase {
   @Override
   public String getDescription() {
      return "При нажатии на экран появляется еще одна точка";
   }

   List<Point> points = new ArrayList<>();

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xff7f7f7f);
      for (Point p : points) {
         canvas.drawCircle(p.x, p.y, 2, paint);
      }
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      points.add(new Point(x, y));
   }
}
