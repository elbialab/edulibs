package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task000 extends TaskBase {

   @Override
   public String getDescription() {
      return "Неверный номер задания";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL_AND_STROKE);
      paint.setColor(0x3fff0000);
      canvas.drawRect(-viewInfo.left, -viewInfo.top, viewInfo.width, viewInfo.height, paint);
   }
}
