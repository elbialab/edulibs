package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import java.util.HashMap;
import java.util.Map;

import ru.elbialab.education.ViewInfo;

public class Task039 extends TaskBase {
   @Override
   public String getDescription() {
      return "При движении по экрану вверх вниз список листается. При нажатии на прямоугольник он остаетсяя зеленым. Список бесконечный";
   }

   private float moveY = 0;
   private float posY = 0;
   private int height = 40;
   private Map<Integer, Boolean> pressed = new HashMap<>();

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL);

      float dp = (float) (moveY + posY - Math.floor((moveY + posY)/height)*height);
      int h = (int)Math.ceil(viewInfo.height)/height + 1;
      for (int i = 0; i <= h; i++) {
         paint.setColor(0x7f7f7f7f);
         float x = -viewInfo.left;
         float y = i*height - dp + viewInfo.top;

         canvas.drawLine(x, y, viewInfo.width + x, y, paint);
         int pos = getPos(moveY + posY + y);
         if (pressed.get(pos) == Boolean.TRUE) {
            paint.setColor(0x7f00ff00);
            canvas.drawRect(x, Math.max(y + 1, -viewInfo.top + 1),
                    viewInfo.width + x, Math.min(y + height - 1, viewInfo.height - viewInfo.top - 1),
                    paint);
         }
         paint.setColor(0x7f0000ff);
         paint.setTextSize(height/2);
         canvas.drawText(Integer.toString(pos), 50, y + height*2/3, paint);
      }
   }

   private int getPos(float y) {
      return (int)Math.floor(y/height);
   }

   private float startY;

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (MotionEvent.ACTION_DOWN == motionEvent) {
          startY = y;
      } else if (MotionEvent.ACTION_MOVE == motionEvent) {
          moveY = startY - y;
      } else if (MotionEvent.ACTION_UP == motionEvent) {
         if (Math.abs(y - startY) < 5) {
            int pos = getPos(startY + moveY + posY);
            pressed.put(pos, pressed.get(pos) != Boolean.TRUE);
         }
         posY = posY + moveY;
         moveY = 0;
      }
   }
}
