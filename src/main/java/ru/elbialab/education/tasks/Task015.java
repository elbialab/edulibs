package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task015 extends TaskBase {
   @Override
   public String getDescription() {
      return "Движение кружка по ломаной линий со скоростью линия за 5 секунд";
   }

   private float t;
   private int sign = 1;
   private float speed = 5;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += speed*sign*dt*2;
      if (t > 2) {
         t = 2;
         sign = -1;
      } else if (t < 0) {
         t = 0;
         sign = 1;
      }
   }

   private float move(float start, float finish, float t) {
      return start + t*(finish - start);
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffcfcfcf);
      canvas.drawLine(30, 20, 100, 100, paint);
      canvas.drawLine(100, 100, 150, 10, paint);

      paint.setColor(0xffffafaf);
      if (t < 1) {
         float x = move(30, 100, t);
         float y = move(20, 100, t);
         canvas.drawCircle(x, y, 10, paint);
      } else {
         float x = move(100, 150, t - 1);
         float y = move(100, 10, t -  1);
         canvas.drawCircle(x, y, 10, paint);
      }
   }
}
