package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task002 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисуй треугольник с кооринатами (30, 40), (50, 100) и (70, 60)";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawLine(30, 40, 50, 100, paint);
      canvas.drawLine(50, 100, 70, 60, paint);
      canvas.drawLine(70, 60, 30, 40, paint);
   }
}
