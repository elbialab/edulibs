package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import ru.elbialab.education.ViewInfo;

public class Task044 extends TaskBase {
   @Override
   public String getDescription() {
      return "Найти точку пересечения функций y = x*x/100 и y = 150cos(x/160*Pi) + 50log(x/10 + 1.1) на отрезке [30, 130]";
   }

   private interface F {
      float f (float x);
   }

   private class GraphShower {
      private F f;

      public GraphShower(F f) {
         this.f = f;
      }

      public void onDraw(Canvas canvas, Paint paint, ViewInfo v) {
         Path path = new Path();
         path.moveTo(-v.left, f.f(-v.left));
         for (float x = -v.left; x < v.width - v.left; x++) {
            path.lineTo(x, f.f(x));
         }
         paint.setColor(0x7f000000 | f.hashCode());
         canvas.drawPath(path, paint);
      }
   }

   private GraphShower g1 = new GraphShower(x -> x*x/100);
   private GraphShower g2 = new GraphShower(x -> (float) (150*Math.cos(x/320*(2*Math.PI)) + Math.log(x/10 + 1.1)*50));

   private float left = 30;
   private float right = 130;
   private float EPS = 1e-4f;
   private float x;
   private float y;

   @Override
   public void onInit(ViewInfo viewInfo) {
      x = solveEquation(g1.f, g2.f, left, right);
      y = g1.f.f(x);
   }

   private float solveEquation(F f1, F f2, float left, float right) {
      F f = x1 -> f1.f(x1) - f2.f(x1);
      float resLeft = f.f(left);
      float resRight = f.f(right);
      while (Math.abs(resLeft - resRight) > EPS) {
         float c = (left + right)/2;
         float resC = f.f(c);

         if ((resLeft < resRight && resC > 0) || (resLeft > resRight && resC < 0)) {
            right = c;
            resRight = resC;
         } else if (Math.abs(resC - resLeft) < EPS || Math.abs(resC - resRight) < EPS) {
            resLeft = resC;
            resRight = resC;
         } else {
            left = c;
            resLeft = resC;
         }
      }

      return (left + right)/2;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      g1.onDraw(canvas, paint, viewInfo);
      g2.onDraw(canvas, paint, viewInfo);

      paint.setColor(0x7f00ff00);
      canvas.drawCircle(x, y, 3, paint);
   }
}
