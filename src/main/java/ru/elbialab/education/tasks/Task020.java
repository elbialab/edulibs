package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task020 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисовать 300 точек в случайном месте, чтобы они не мерцали";
   }

   private class Point {
      float x;
      float y;

      public Point(float x, float y) {
         this.x = x;
         this.y = y;
      }
   }

   private Random rnd = new Random();
   private Point[] points = new Point[300];

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0x3fff0000);
      for (int i = 0; i < points.length; i++) {
         if (points[i] == null) {
            points[i] = new Point(rnd.nextFloat()*viewInfo.width - viewInfo.left, rnd.nextFloat()*viewInfo.height - viewInfo.top);
         }

         canvas.drawCircle(points[i].x, points[i].y, 1, paint);
      }
   }
}
