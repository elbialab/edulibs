package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task037 extends TaskBase {
   @Override
   public String getDescription() {
      return "Анимация процесса сортировки";
   }

   private Random random = new Random();
   private float t;
   private int step;

   private int[] array;
   private int count = 100;
   private int current = 0;
   private int pos0 = -1;
   private int pos1 = -1;
   private boolean before = false;

   @Override
   public void onInit(ViewInfo viewInfo) {
      int[] init = new int[count];
      for (int i = 0; i < init.length; i++) {
         init[i] = i;
      }

      array = new int[count];
      for (int i = 0; i < array.length; i++) {
         int pos = random.nextInt(array.length - i);
         array[i] = init[pos];
         init[pos] = init[array.length - i - 1];
      }
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += dt;
      if (t*20 > step) {
         step++;

         if (!before) {
            current++;

            if (current >= array.length) {
               pos0 = pos1 = -1;
               return;
            }

            pos0 = current - 1;
            int posMin = current;
            int minValue = array[posMin];
            for (int i = current - 1; i < array.length; i++) {
               if (array[i] < minValue) {
                  minValue = array[i];
                  posMin = i;
               }
            }
            pos1 = posMin;
            before = true;
         } else {
            before = false;
            if (pos0 != pos1) {
               int temp = array[pos0];
               array[pos0] = array[pos1];
               array[pos1] = temp;
            }
         }
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL);
      paint.setColor(0x7f7f7f7f);
      float dx = (viewInfo.width - viewInfo.left)/count;
      for (int i = 0; i < array.length; i++) {
         if (i == pos0 || i == pos1) {
            continue;
         }
         canvas.drawRect(i*dx, 100 - array[i] + 5, i*dx + dx - 0.5f, 105, paint);
      }

      if (pos0 != -1 && pos1 != -1) {
         paint.setColor(before ? 0x7f007f00 : 0x7f00ff00);
         canvas.drawRect(pos0 * dx, 100 - array[pos0] + 5, pos0 * dx + dx - 0.5f, 105, paint);
         paint.setColor(before ? 0x7f00007f : 0x7f0000ff);
         canvas.drawRect(pos1 * dx, 100 - array[pos1] + 5, pos1 * dx + dx - 0.5f, 105, paint);
      }
   }
}
