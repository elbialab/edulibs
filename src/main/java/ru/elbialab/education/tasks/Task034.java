package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task034 extends TaskBase {
   @Override
   public String getDescription() {
      return "Дан треугольник. На каждом шаге у каждого треугольника вырезается середина. Получается три треугольника.\nОтобразить 6 шагов";
   }

   Point[] src = {new Point(30, 40), new Point(150, 60), new Point(80, 150)};
   float t = 0;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t = t + 5*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      int iteration = Math.min(Math.round(t), 5);
      paint.setColor(0xff7f7f7f);
      paint.setStyle(Paint.Style.FILL);
      process(iteration, canvas, paint, src);
   }

   private Point getPointBetween(Point a, Point b) {
      return new Point((a.x + b.x)/2, (a.y + b.y)/2);
   }

   private void process(int iteration, Canvas canvas, Paint paint, Point ... src) {
      if (iteration <= 0) {
         Path path = new Path();
         path.moveTo(src[0].x, src[0].y);
         path.lineTo(src[1].x, src[1].y);
         path.lineTo(src[2].x, src[2].y);
         path.close();
         canvas.drawPath(path, paint);
         return;
      }

      Point dist[] = {getPointBetween(src[1], src[2]), getPointBetween(src[2], src[0]), getPointBetween(src[0], src[1])};
      process(iteration - 1, canvas, paint, src[0], dist[1], dist[2]);
      process(iteration - 1, canvas, paint, dist[0], src[1], dist[2]);
      process(iteration - 1, canvas, paint, dist[0], dist[1], src[2]);
   }
}
