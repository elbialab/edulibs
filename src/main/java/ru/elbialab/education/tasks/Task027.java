package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task027 extends TaskBase {
   @Override
   public String getDescription() {
      return "Разноцветные фигуры";
   }

   private Random rnd = new Random();
   private Figure[] figures;

   abstract class Figure {
      float x;
      float y;
      float width;
      float height;
      int color;

      Figure(float x, float y, float width, float height) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.height = height;
         color = 0xff000000 | rnd.nextInt();
      }

      void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         drawFigure(canvas, paint);
      }

      abstract void drawFigure(Canvas canvas, Paint paint);
   }

   class Circle extends Figure {

      float radius;
      float cx;
      float cy;

      public Circle(float x, float y, float width, float height) {
         super(x, y, width, height);
         cx = x + width/2;
         cy = y + height/2;
         radius = Math.min(width/2, height/2);
      }

      @Override
      void drawFigure(Canvas canvas, Paint paint) {
         canvas.drawCircle(cx, cy, radius, paint);
      }
   }

   class Triangle extends Circle {
      Point p0;
      Point p1;
      Point p2;

      public Triangle(float x, float y, float width, float height) {
         super(x, y, width, height);
         p0 = new Point(cx + radius*Math.cos(0*2*Math.PI/3), cy + radius*Math.sin(0*2*Math.PI/3));
         p1 = new Point(cx + radius*Math.cos(1*2*Math.PI/3), cy + radius*Math.sin(1*2*Math.PI/3));
         p2 = new Point(cx + radius*Math.cos(2*2*Math.PI/3), cy + radius*Math.sin(2*2*Math.PI/3));
      }

      @Override
      void drawFigure(Canvas canvas, Paint paint) {
         Path path = new Path();
         path.moveTo(p0.x, p0.y);
         path.lineTo(p1.x, p1.y);
         path.lineTo(p2.x, p2.y);
         path.lineTo(p0.x, p0.y);
         canvas.drawPath(path, paint);
      }
   }

   class Rect extends Figure {
      public Rect(float x, float y, float width, float height) {
         super(x, y, width, height);
      }

      @Override
      void drawFigure(Canvas canvas, Paint paint) {
         canvas.drawRect(x, y, x + width, y + height, paint);
      }
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      figures = new Figure[9];
      for (int i = 0; i < figures.length; i++) {
         int fig = rnd.nextInt(3);
         Figure figure = fig == 0
                 ? new Rect(i % 3 * 60 + 10, i / 3 * 60 + 10, 50, 50)
                 : fig == 1
                    ? new Circle(i % 3 * 60 + 10, i / 3 * 60 + 10, 50, 50)
                    : new Triangle(i % 3 * 60 + 10, i / 3 * 60 + 10, 50, 50);
         figures[i] = figure;
      }
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL_AND_STROKE);
      for (Figure square : figures) {
         square.onDraw(canvas, paint);
      }
   }
}
