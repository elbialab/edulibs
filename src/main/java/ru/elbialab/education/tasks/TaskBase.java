package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.Drawer;
import ru.elbialab.education.ViewInfo;

public abstract class TaskBase extends Drawer {
   public abstract String getDescription();

   public final int getTaskNumber() {
      return Integer.valueOf(this.getClass().getSimpleName().replace("Task", ""));
   }

   @Override
   public void onDraw(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      preview(canvas, paint, viewInfo);
   }

   protected abstract void preview(Canvas canvas, Paint paint, ViewInfo viewInfo);
}
