package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task030 extends TaskBase {
   @Override
   public String getDescription() {
      return "Написать текст \"Hello world!\"";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xff7f7f7f);
      paint.setStyle(Paint.Style.FILL);
      paint.setTextSize(10);
      canvas.drawText("Hello world!", 10, 10, paint);
   }
}
