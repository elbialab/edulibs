package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import ru.elbialab.education.ViewInfo;

public class Task049 extends TaskBase {
   private Random rnd = new Random();
   private List<Square> sqs = new ArrayList<>();
   private ReentrantReadWriteLock rw = new ReentrantReadWriteLock();

   @Override
   public String getDescription() {
      return "Создать 100 потоков. Каждый поток отображать квадратом:\n" +
              "зеленого цвета - поток завершился;\n" +
              "желтого цвета - поток выполняется;\n" +
              "красного цвета - поток ожидает выполнения;\n" +
              "серого цвета - поток еще не запущен.\n" +
              "20 процентов поток писатели, остальные читатели. Читатели могут читать из критической секции, " +
              "пока ее не занял писатель. Писатель может писать в критическую секцию, когда нет ни одного читателя. " +
              "Писатели пишут за время от 1000 до 3000мс. Читатели читают за время от 500 до 1500мс";
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      for (int x = 0; x < 10; x++) {
         for (int y = 0; y < 10; y++) {
            Square s = new Square(x * 30, y * 30, 30, rnd.nextInt(100) < 20);
            sqs.add(s);
         }
      }

      for (Square sq : sqs) {
         sq.thread.start();
      }
   }

   private void criticalWrite(long waitTime) {
      rw.writeLock().lock();
      try {
         try {
            Thread.sleep(waitTime);
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
      } finally {
         rw.writeLock().unlock();
      }
   }

   private void criticalRead(long waitTime) {
      rw.readLock().lock();
      try {
         try {
            Thread.sleep(waitTime);
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
      } finally {
         rw.readLock().unlock();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (Square s : sqs) {
         s.draw(canvas, paint);
      }
   }

   private class Square {
      private float x;
      private float y;
      private float width;
      private boolean writer;
      private long waitTime;
      private Thread thread;

      public Square(float x, float y, float width, boolean writer) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.writer = writer;
         if (writer) {
            this.waitTime = 1000 + rnd.nextInt(2000);
         } else {
            this.waitTime = 500 + rnd.nextInt(1000);
         }
         thread = new Thread(() -> {
            if (writer) {
               criticalWrite(waitTime);
            } else {
               criticalRead(waitTime);
            }
         });
         thread.setUncaughtExceptionHandler((t, e) -> e.printStackTrace());
      }

      public void draw(Canvas canvas, Paint paint) {
         paint.setStyle(Paint.Style.FILL);
         switch (thread.getState()) {
            case NEW:
               paint.setColor(0x7f7f7f7f);
               break;
            case RUNNABLE:
            case TIMED_WAITING:
               paint.setColor(0x7fffff00);
               break;
            case WAITING:
            case BLOCKED:
               paint.setColor(0x7fff0000);
               break;
            case TERMINATED:
               paint.setColor(0x7f00ff00);
               break;
         }
         canvas.drawRect(x, y, x + width, y + width, paint);

         paint.setTextSize(width * 0.4f);
         paint.setStyle(Paint.Style.STROKE);
         paint.setColor(0x7f000000);
         paint.setStrokeWidth(1);
         StringBuilder sb = new StringBuilder();
         if (writer) {
            sb.append("w");
         } else {
            sb.append("R");
         }
         canvas.drawText(sb.toString(), x + 2, y + width * 0.8f, paint);
      }
   }
}
