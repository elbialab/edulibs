package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import ru.elbialab.education.ViewInfo;

public class Task043 extends TaskBase {
   private final int cellSize = 4;
   private static final float minRadius = 2;
   private static final float maxRadius = 100;
   private int width;
   private final Random rnd = new Random();
   private List<Circle> circles = new ArrayList<>();
   private SparseArray<List<Circle>> distToZeroToCircles = new SparseArray<>();
   private List<Integer> free = new ArrayList<>();
   private int[] posToFree;
   private long initTime;
   private boolean inited = false;

   @Override
   public String getDescription() {
      return "Замостить экран кругами разного радиуса. Минимальный радиус 2, а максимальный 100";
   }

   private int getCellIndex(float x, float y) {
      int tx = (int) Math.floor(x);
      int ty = (int) Math.floor(y);
      tx /= cellSize;
      ty /= cellSize;
      return tx + ty * width;
   }

   private int getXFromIndex(int index) {
      return index % width;
   }

   private int getYFromIndex(int index) {
      return index / width;
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      circles = new ArrayList<>();
      distToZeroToCircles = new SparseArray<>();
      free = new ArrayList<>();

      long start = System.currentTimeMillis();
      width = (int) Math.ceil(viewInfo.width) / cellSize;
      int maxIndex = getCellIndex(viewInfo.width, viewInfo.height);
      posToFree = new int[maxIndex];
      for (int i = 0; i < maxIndex; i++) {
         free.add(i);
         posToFree[i] = i;
      }

      addCircle(viewInfo);
      while (!free.isEmpty()) {
         addCircle(viewInfo);
      }

      for (Circle c : circles) {
         c.x -= viewInfo.left;
         c.y -= viewInfo.top;
      }
      initTime = System.currentTimeMillis() - start;
      inited = true;
   }

   private void addCircle(ViewInfo v) {
      int pos = rnd.nextInt(free.size());
      int index = free.get(pos);
      free.set(pos, free.get(free.size() - 1));
      free.remove(free.size() - 1);
      posToFree[pos] = -1;

      int tx = getXFromIndex(index);
      int ty = getYFromIndex(index);
      float cx = (tx + rnd.nextFloat()) * cellSize;
      float cy = (ty + rnd.nextFloat()) * cellSize;
      int rtz = (int) Math.round(Math.sqrt(cx * cx + cy * cy));
      int r = (int) Math.ceil(Math.min(
              Math.min(cx, v.width - cx),
              Math.min(cx, v.height - cy)
      ));
      if (r <= 0) {
         return;
      }

      Circle nc = new Circle(cx, cy, r);
      Set<Circle> processed = new HashSet<>();
      for (int i = rtz - r - 1; i <= rtz + r + 1; i++) {
         List<Circle> cl = distToZeroToCircles.get(i);
         if (cl == null) {
            continue;
         }
         for (Circle c : cl) {
            if (!processed.contains(c) && !c.far(nc)) {
               float nr = (float) Math.sqrt((c.x - nc.x) * (c.x - nc.x) + (c.y - nc.y) * (c.y - nc.y)) - c.radius;
               nc = new Circle(cx, cy, nr);
            }
            processed.add(c);
         }
      }

      if (minRadius < nc.radius && nc.radius <= maxRadius) {
         circles.add(nc);
         for (float x = Math.max(0, (nc.x - nc.radius)); x <= (nc.x + nc.radius); x += cellSize) {
            for (float y = Math.max(0, (nc.y - nc.radius)); y <= (nc.y + nc.radius); y += cellSize) {
               if ((nc.x - x) * (nc.x - x) + (nc.y - y) * (nc.y - y) < nc.radius * nc.radius) {
                  pos = getCellIndex(x, y);
                  index = posToFree[pos];

                  if (index >= 0) {
                     if (index < free.size()) {
                        free.set(index, free.get(free.size() - 1));
                        free.remove(free.size() - 1);
                     }
                     posToFree[pos] = -1;
                  }
               }
            }
         }
         rtz = nc.getDistToZero();
         for (int tr = Math.round(rtz - nc.radius) - 1; tr <= Math.round(rtz + nc.radius) + 1; tr++) {
            List<Circle> list = distToZeroToCircles.get(tr);
            if (list != null) {
               list.add(nc);
            } else {
               distToZeroToCircles.put(tr, new ArrayList<>(Collections.singletonList(nc)));
            }
         }
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      if (!inited) {
         return;
      }

      for (Circle circle : circles) {
         circle.onDraw(canvas, paint);
      }

      paint.setColor(0xff000000);
      paint.setTextSize(10);
      canvas.drawText(circles.size() * 1000 / initTime + "cps", 15, 15, paint);
   }

   private class Circle {
      private final int color;
      private final float radius;
      private float x;
      private float y;

      public Circle(float x, float y, float radius) {
         this.color = (0x7f000000 | rnd.nextInt()) & (0x7fffffff);
         this.x = x;
         this.y = y;
         this.radius = radius;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawCircle(x, y, radius, paint);
      }

      public boolean far(Circle c) {
         return (c.x - x) * (c.x - x) + (c.y - y) * (c.y - y) > (radius + c.radius) * (radius + c.radius);
      }

      public int getDistToZero() {
         return (int) Math.round(Math.sqrt(x * x + y * y));
      }
   }
}
