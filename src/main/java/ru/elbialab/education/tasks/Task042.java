package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task042 extends TaskBase {
   @Override
   public String getDescription() {
      return "Игра-бродилка: крудок движется по клеточкам с помощью кнопок. " +
              "При попадании на красную клетку, программа аварийно завершается";
   }

   private Random rnd = new Random();
   private Cell[][] cells = new Cell[10][10];
   private List<JoyStick> joyStick = new ArrayList<>();
   private Ball ball;
   private String message;

   private class Ball {
      private int color;
      private float x;
      private float y;
      private int i;
      private int j;
      private float radius;

      public Ball(float x, float y) {
         this.color = 0x7fffff00;
         this.x = x;
         this.y = y;
         this.radius = 10;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawCircle(x, y, radius,  paint);
      }
   }

   private class Cell {
      private int color;
      private int x;
      private int y;
      private int width;
      private int height;

      public Cell(int color, float x, float y, float width, float height) {
         this.color = color;
         this.x = (int) x;
         this.y = (int) y;
         this.width = (int) width;
         this.height = (int) height;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(color);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawRect(x, y, x + width, y + height, paint);
      }
   }

   private class JoyStick {
      private int x;
      private float y;
      private int width;
      private int height;
      private Runnable command;

      public JoyStick(int x, float y, int width, int height, Runnable command) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.height = height;
         this.command = command;
      }

      public boolean collapse(float x, float y) {
         return this.x <= x && x <= this.x + width && this.y <= y && y <= this.y + height;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(0x7f000000);
         paint.setStyle(Paint.Style.FILL);
         canvas.drawRect(x, y, x + width, y + height, paint);
      }
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      for (int i = 0; i < cells.length; i++) {
         for (int j = 0; j < cells[i].length; j++) {
            int color = rnd.nextInt(10) < 8 ? 0x7f00ff00 : 0x7fff0000;

            float w = viewInfo.width/cells.length;
            float h = (viewInfo.height - 110)/cells[0].length*2/3;
            Cell cell = new Cell(color,
                    i * w - viewInfo.left, j * h - viewInfo.top,
                    w, h);
            cells[i][j] = cell;

            if (color == 0x7f00ff00 && ball == null) {
               ball = new Ball(cell.x + w/2, cell.y + h/2);
               ball.i = i;
               ball.j = j;
            }
         }
      }

      float y = cells[9][9].y + cells[9][9].height + 20;
      JoyStick left = new JoyStick(10, y + 50, 50, 50, new Command() {
         @Override
         public void run() {
            ball.i--;
            if (ball.i < 0) {
               ball.i = 0;
            }
            super.run();
         }
      });
      joyStick.add(left);
      JoyStick right = new JoyStick(10 + 50*2, y + 50, 50, 50, new Command() {
         @Override
         public void run() {
            ball.i++;
            if (ball.i > 9) {
               ball.i = 9;
            }
            super.run();
         }
      });
      joyStick.add(right);
      JoyStick top = new JoyStick(10 + 50, y, 50, 50, new Command() {
         @Override
         public void run() {
            ball.j--;
            if (ball.j < 0) {
               ball.j = 0;
            }
            super.run();
         }
      });
      joyStick.add(top);
      JoyStick bottom = new JoyStick(10 + 50, y + 50*2, 50, 50, new Command() {
         @Override
         public void run() {
            ball.j++;
            if (ball.j > 9) {
               ball.j = 9;
            }
            super.run();
         }
      });
      joyStick.add(bottom);
   }

   private abstract class Command implements Runnable {
      @Override
      public void run() {
         Cell cell = cells[ball.i][ball.j];
         ball.x = cell.x + cell.width/2f;
         ball.y = cell.y + cell.height/2f;
         if ((cell.color & 0x00ff0000) == 0x00ff0000) {
            throw new RuntimeException("Crash!!!");
         }
      }
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      try {
         if (motionEvent == MotionEvent.ACTION_UP) {
            message = null;
            for (JoyStick joy : joyStick) {
               if (joy.collapse(x, y)) {
                  joy.command.run();
               }
            }
         }
      } catch (Exception e) {
         message = e.getMessage();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);

      for (Cell[] cell2 : cells) {
         for (Cell cell : cell2) {
            cell.onDraw(canvas, paint);
         }
      }

      for (JoyStick j : joyStick) {
         j.onDraw(canvas, paint);
      }

      ball.onDraw(canvas, paint);

      String m = message;
      if (m != null) {
         paint.setColor(0x7fff0000);
         paint.setTextSize(20);
         canvas.drawText(m, 10, 10, paint);
      }
   }
}
