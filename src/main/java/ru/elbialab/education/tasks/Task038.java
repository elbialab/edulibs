package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task038 extends TaskBase {
   @Override
   public String getDescription() {
      return "При движении по экрану точка также движется";
   }

   private Point p = new Point(100, 100);
   private Point move = new Point(0, 0);

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL);
      paint.setColor(0x7fff0000);

      canvas.drawCircle(p.x + move.x, p.y + move.y, 20, paint);
   }

   private float lx, ly;

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (MotionEvent.ACTION_DOWN == motionEvent) {
          lx = x;
          ly = y;
      } else if (MotionEvent.ACTION_MOVE == motionEvent) {
          move.x += x - lx;
          move.y += y - ly;
          lx = x;
          ly = y;
      }
   }
}
