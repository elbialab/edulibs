package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import ru.elbialab.education.ViewInfo;

public class Task036 extends TaskBase {
   @Override
   public String getDescription() {
      return "Игра: найди одинаковые";
   }

   private int[] colors = {Color.BLUE, Color.GREEN, Color.RED, Color.YELLOW, Color.CYAN, Color.MAGENTA};
   private Random rnd = new Random();
   private Square[] squares;
   private int countOfSuccess = 0;
   private int firstSelected = -1;
   private int secondSelected = -1;
   private List<Point> lifes = new ArrayList<>();
   private boolean endOfGame = false;
   private boolean win = false;

   class Square {
      float x;
      float y;
      float width;
      float height;
      int color;
      boolean pressed = false;
      boolean hide = false;
      float t = 0;

      public Square(float x, float y, float width, float height) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.height = height;
      }

      public void onDraw(Canvas canvas, Paint paint) {
         paint.setColor(pressed || hide ? color : 0x7f7f7f7f);
         canvas.drawRect(x, y, x + width, y + height, paint);
      }

      public void onUpdate(float dt) {
         if (pressed) {
            t += 20*dt;
            if (t > 3) {
               pressed = false;
               t = 0;
            }
         }
         if (hide && !pressed) {
            t += 20*dt;
            if (t > 1) {
               t = 1;
            }
            color = Math.round(0x7f*(1 - t)) << 24 | (0x00ffffff & color);
         }
      }

      public boolean collapse(float x, float y) {
         return this.x <= x && x <= this.x + width && this.y <= y && y <= this.y + height;
      }

      public void setPressed(boolean pressed) {
         if (hide) {
            return;
         }
         this.pressed = pressed;
         t = 0;
      }
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      squares = new Square[16];
      List<Integer> empty = new ArrayList<>();
      for (int i = 0; i < squares.length; i++) {
         squares[i] = new Square(i % 4 * 40 + 10, i / 4 * 50 + 25, 30, 40);
         empty.add(i);
      }

      for (int i = 0; i < squares.length; i+=2) {
         int color = colors[rnd.nextInt(colors.length)];

         int p0 = rnd.nextInt(empty.size());
         p0 = empty.remove(p0);

         int p1 = rnd.nextInt(empty.size());
         p1 = empty.remove(p1);

         squares[p0].color = color;
         squares[p1].color = color;
      }

      for (int i = 0; i < 8; i++) {
         lifes.add(new Point(viewInfo.width - i*25 - 25, 10));
      }
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      for (Square square : squares) {
         square.onUpdate(dt);
      }

      if (firstSelected > -1 && !squares[firstSelected].pressed) {
         firstSelected = -1;
      }

      if (secondSelected > -1 && !squares[secondSelected].pressed) {
         secondSelected = -1;
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setStyle(Paint.Style.FILL_AND_STROKE);
      for (Square square : squares) {
         square.onDraw(canvas, paint);
      }
      for (Point p : lifes) {
         paint.setColor(0x7fff0000);
         canvas.drawCircle(p.x, p.y, 10, paint);
      }
      if (endOfGame) {
         paint.setTextSize(30);
         paint.setColor(win ? 0x7f00ff00 : 0x7fff0000);
         canvas.drawText(win ? "YOU WIN" : "YOU LOSE", 50, 100, paint);
      }
   }

   @Override
   public void onTouch(float x, float y, int motionEvent) {
      if (endOfGame || (firstSelected >= 0 && secondSelected >= 0)) {
         return;
      }
      if (motionEvent == MotionEvent.ACTION_DOWN) {
         int i = 0;
         for (Square square : squares) {
            if (square.collapse(x, y)) {
               square.pressed = true;
               if (firstSelected == -1) {
                  firstSelected = i;
               } else if (secondSelected == -1 && i != firstSelected) {
                  secondSelected = i;
                  Square second = squares[firstSelected];
                  if (square.color == second.color) {
                     square.hide = true;
                     second.hide = true;
                     square.t = 2;
                     second.t = 2;
                     countOfSuccess+=2;
                     if (countOfSuccess == squares.length) {
                        endOfGame = true;
                        win = true;
                     }
                  } else {
                     lifes.remove(lifes.size() - 1);
                     if (lifes.isEmpty()) {
                        endOfGame = true;
                        win = false;
                        square.t = 2;
                        second.t = 2;
                     }
                  }
               }
            }
            i++;
         }
      }
   }
}
