package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task021 extends TaskBase {
   @Override
   public String getDescription() {
      return "Точка движется в случайном направлении (-100 до 100 разброс направлений) из центра экрана";
   }

   private class Point {
      float x;
      float y;

      public Point(float x, float y) {
         this.x = x;
         this.y = y;
      }
   }

   private Random rnd = new Random();
   private Point vector;
   private Point p;

   @Override
   public void onInit(ViewInfo viewInfo) {
      vector = new Point(rnd.nextFloat()*200 - 100, rnd.nextFloat()*200 - 100);
      p = new Point(viewInfo.width/2, viewInfo.height/2);
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      super.onUpdate(dt, viewInfo);
      p.x += vector.x*dt;
      p.y += vector.y*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0x3f0000ff);
      canvas.drawCircle(p.x, p.y, 2, paint);
   }
}
