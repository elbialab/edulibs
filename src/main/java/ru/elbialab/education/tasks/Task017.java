package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task017 extends TaskBase {
   @Override
   public String getDescription() {
      return "Нарисовать 20 линий согласно образцу";
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      for (int i = 0; i < 20; i++) {
         canvas.drawLine(30, 20 + i*10, 100, 30 + i*10, paint);
      }
   }
}
