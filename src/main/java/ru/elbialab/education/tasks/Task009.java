package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task009 extends TaskBase {
   @Override
   public String getDescription() {
      return "Треугольник (20, 30) (40, 150) (140, 80) превращается в треугольник (200, 100) (130, 20) (60, 180)";
   }

   private Point p1 = new Point();
   private Point p2 = new Point();
   private Point p3 = new Point();
   private Vector dir1 = new Vector();
   private Vector dir2 = new Vector();
   private Vector dir3 = new Vector();

   @Override
   public void onInit(ViewInfo viewInfo) {
      p1.x = 20; p1.y = 30;
      p2.x = 40; p2.y = 150;
      p3.x = 140; p3.y = 80;

      dir1.x = 200 - 20; dir1.y = 100 - 30;
      dir2.x = 130 - 40; dir2.y = 20 - 150;
      dir3.x = 60 - 140; dir3.y = 180 - 80;
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      p1.x += dir1.x*dt;
      p1.y += dir1.y*dt;

      p2.x += dir2.x*dt;
      p2.y += dir2.y*dt;

      p3.x += dir3.x*dt;
      p3.y += dir3.y*dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffafafaf);
      canvas.drawCircle(20, 30, 2, paint);
      canvas.drawCircle(40, 150, 2, paint);
      canvas.drawCircle(140, 80, 2, paint);
      canvas.drawCircle(200, 100, 2, paint);
      canvas.drawCircle(130, 20, 2, paint);
      canvas.drawCircle(60, 180, 2, paint);

      paint.setColor(0xff007f00);
      canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
      canvas.drawLine(p3.x, p3.y, p2.x, p2.y, paint);
      canvas.drawLine(p1.x, p1.y, p3.x, p3.y, paint);
   }
}
