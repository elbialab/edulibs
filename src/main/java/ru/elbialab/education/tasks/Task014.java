package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.elbialab.education.ViewInfo;

public class Task014 extends TaskBase {
   @Override
   public String getDescription() {
      return "Движение кружка по прямой. Всю прямую кружок пройдет за 8 секунд";
   }

   private float t;
   private int sign = 1;
   private float speed = 8;
   float x = 30;
   float y = 20;

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += speed*sign*dt;
      if (t > 1) {
         t = 1;
         sign = -1;
      } else if (t < 0) {
         t = 0;
         sign = 1;
      }

      x = 30 + t*(200 - 30);
      y = 20 + t*(300 - 20);
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xffffafaf);
      canvas.drawCircle(x, y, 2, paint);
   }
}
