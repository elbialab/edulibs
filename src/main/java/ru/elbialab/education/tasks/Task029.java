package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import ru.elbialab.education.ViewInfo;

import static java.lang.Math.PI;
import static java.lang.Math.log;
import static java.lang.Math.sqrt;

public class Task029 extends TaskBase {
   @Override
   public String getDescription() {
      return "График функции y = 50*sin(sqrt(3)*(x+100*t)/PI/4) + 50*Math.cos(sqrt(2)*x/PI/6)*log(t + 1) +100);";
   }

   private float t = 0;

   @Override
   public void onInit(ViewInfo viewInfo) {
   }

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      t += dt;
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(0xff7f7f7f);
      paint.setStrokeWidth(2);

      Point last = new Point(-viewInfo.left, f(-viewInfo.left));
      for (float i = 0.5f; i <= viewInfo.width; i+=0.5f) {
         Point cur = new Point(i - viewInfo.left , f(i - viewInfo.left));
         canvas.drawLine(last.x, last.y, cur.x, cur.y, paint);
         last = cur;
      }
   }

   private float f(float x) {
      return (float) (50*Math.sin(sqrt(3)*(x+100*t)/PI/4) + 50*Math.cos(sqrt(2)*x/PI/6)*log(t + 1) +100);
   }
}
