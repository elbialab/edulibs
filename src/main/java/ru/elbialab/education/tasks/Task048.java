package ru.elbialab.education.tasks;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.elbialab.education.ViewInfo;

public class Task048 extends TaskBase {
   private Random rnd = new Random();
   private List<Square> sqs = new ArrayList<>();
   private final Object WAITER = new Object();

   @Override
   public String getDescription() {
      return "Создать 40 потоков. Каждый поток отображать квадратом:\n" +
              "зеленого цвета - поток завершился;\n" +
              "желтого цвета - поток выполняется;\n" +
              "красного цвета - поток ожидает выполнения;\n" +
              "серого цвета - поток еще не запущен.\n" +
              "Потоки стартуют все сразу. Случайная половина выполняет случайное ожидание от 750 до 2000мс, либо бесконечно," +
              " а вторая часть - ждет случайное время от 750 до 2000мс," +
              " а потом уведомляет либо все либо один поток и необходимости продолжить выполенние";
   }

   @Override
   public void onInit(ViewInfo viewInfo) {
      for (int x = 0; x < 10; x++) {
         for (int y = 0; y < 4; y++) {
            Square s = new Square(x * 30, y * 30, 30,
                    rnd.nextBoolean(), rnd.nextInt(2000), rnd.nextBoolean());
            sqs.add(s);
         }
      }

      for (Square sq : sqs) {
         sq.thread.start();
      }
   }

   @Override
   protected void preview(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      for (Square s : sqs) {
         s.draw(canvas, paint);
      }
   }

   private class Square {
      private float x;
      private float y;
      private float width;
      private boolean waiter;
      private long waitTime;
      private Thread thread;
      private boolean notifyAll;

      public Square(float x, float y, float width, boolean waiter, long waitTime, boolean notifyAll) {
         this.x = x;
         this.y = y;
         this.width = width;
         this.waiter = waiter;
         this.waitTime = waitTime > 1250 ? -1 : waitTime;
         this.notifyAll = notifyAll;
         thread = new Thread(() -> {
            try {
               if (waiter) {
                  synchronized (WAITER) {
                     if (waitTime < 0) {
                        WAITER.wait();
                     } else {
                        WAITER.wait(750 + waitTime);
                     }
                  }
               } else if (notifyAll) {
                  Thread.sleep(750 + waitTime);
                  synchronized (WAITER) {
                     WAITER.notifyAll();
                  }
               } else {
                  Thread.sleep(750 + waitTime);
                  synchronized (WAITER) {
                     WAITER.notify();
                  }
               }
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
         });
      }

      public void draw(Canvas canvas, Paint paint) {
         paint.setStyle(Paint.Style.FILL);
         switch (thread.getState()) {
            case NEW:
               paint.setColor(0x7f7f7f7f);
               break;
            case RUNNABLE:
            case TIMED_WAITING:
               paint.setColor(0x7fffff00);
               break;
            case WAITING:
            case BLOCKED:
               paint.setColor(0x7fff0000);
               break;
            case TERMINATED:
               paint.setColor(0x7f00ff00);
               break;
         }
         canvas.drawRect(x, y, x + width, y + width, paint);

         paint.setTextSize(width*0.4f);
         paint.setStyle(Paint.Style.STROKE);
         paint.setColor(0x7f000000);
         paint.setStrokeWidth(1);
         StringBuilder sb = new StringBuilder();
         if (waiter) {
            sb.append("w");
            if (waitTime < 0) {
               sb.append("I");
            }
         } else {
            sb.append("N");
            if (notifyAll) {
               sb.append("A");
            }
         }
         canvas.drawText(sb.toString(), x, y + width*0.8f, paint);
      }
   }
}
