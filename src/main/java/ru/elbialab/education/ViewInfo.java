package ru.elbialab.education;

import android.content.Context;

public class ViewInfo {
   public ViewInfo(float height, float width) {
      this.height = height;
      this.width = width;
   }

   public float width;
   public float height;
   public float left;
   public float top;
   public Context context;
}
